package com.example.plugName;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlugNameApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlugNameApplication.class, args);
	}

}
