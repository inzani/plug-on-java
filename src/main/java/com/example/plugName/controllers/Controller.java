package com.example.plugName.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class Controller {
    @PostMapping(value = "/postXML",consumes = "application/xml", produces = "application/xml")
    public String postXML(@RequestBody String xml) {
        return processingXmlFile(xml);
    }

    private static String processingXmlFile(String xml) {
        String regexElement = "<date>(.*)</date>";
        Pattern p = Pattern.compile(regexElement);
        Matcher m = p.matcher(xml);

        if(m.find()){
            int groupNumber = 1;
            String date = m.group(groupNumber);
            String dateConvert = workingWithDate(m.group(groupNumber)).toString();
            return xml.replace(date,dateConvert);
        }
        return "Error";
    }

    private static LocalDate workingWithDate(String date) {
        int numberDays = 1;
        String RegexDate = "yyyy-MM-dd";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(RegexDate);
        LocalDate dateParse = LocalDate.parse(date, formatter);
        return dateParse.plusDays(numberDays);
    }
}
